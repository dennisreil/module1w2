type Voter = string;
type Candidate = string;

function registerVoter<V>(voter: V, voterSet: Set<V>): Set<V> {
  // your code here
    voterSet.add(voter);
    return voterSet;
}

function castVote<V, C>(voter: V, candidate: C, voteMap: Map<V, C>, voterSet: Set<V>): Map<V, C> {
    // your code here
        if (voteMap.has(voter)) {
            return voteMap;
        }
        voteMap.set(voter, candidate);
        return voteMap;
}

//////////////////////////////////////////////////////////////////////////////
//----------------------- DO NOT EDIT CODE BELOW ---------------------------//
//////////////////////////////////////////////////////////////////////////////

// Create a set of voters and a map of votes
let voters = new Set<Voter>();
let votes = new Map<Voter, Candidate>();

// Register several voters
voters = registerVoter("Voter1", voters);
voters = registerVoter("Voter2", voters);
voters = registerVoter("Voter3", voters);

// Attempt to register the same voter twice
voters = registerVoter("Voter1", voters);

// Cast votes for several candidates
votes = castVote("Voter1", "Candidate1", votes, voters);
votes = castVote("Voter2", "Candidate2", votes, voters);
votes = castVote("Voter3", "Candidate3", votes, voters);

// Attempt to cast a vote twice with the same voter
votes = castVote("Voter1", "Candidate4", votes, voters);
console.log(votes);
