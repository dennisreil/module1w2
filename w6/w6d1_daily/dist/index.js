"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function registerVoter(voter, voterSet) {
    // your code here
    voterSet.add(voter);
    return voterSet;
}
function castVote(voter, candidate, voteMap, voterSet) {
    // your code here
    if (voteMap.has(voter)) {
        return voteMap;
    }
    voteMap.set(voter, candidate);
    return voteMap;
}
//////////////////////////////////////////////////////////////////////////////
//----------------------- DO NOT EDIT CODE BELOW ---------------------------//
//////////////////////////////////////////////////////////////////////////////
// Create a set of voters and a map of votes
let voters = new Set();
let votes = new Map();
// Register several voters
voters = registerVoter("Voter1", voters);
voters = registerVoter("Voter2", voters);
voters = registerVoter("Voter3", voters);
// Attempt to register the same voter twice
voters = registerVoter("Voter1", voters);
// Cast votes for several candidates
votes = castVote("Voter1", "Candidate1", votes, voters);
votes = castVote("Voter2", "Candidate2", votes, voters);
votes = castVote("Voter3", "Candidate3", votes, voters);
// Attempt to cast a vote twice with the same voter
votes = castVote("Voter1", "Candidate4", votes, voters);
console.log(votes);
//# sourceMappingURL=index.js.map