Requirements

Add the Radio interface.

Create a new class named CarRadio that implements the Radio interface.

Add a private property that is a boolean called isOn to manage the radio's state (on or off).

Add a private property that is a number called currentStation to hold the station number to which the radio is currently tuned.

Add methods called powerOn and powerOff that set the isOn property to true and false, respectively.

Add a setStation method that accepts a number as an argument and sets the currentStation property to the value of the argument.

Add a new method called getStation that returns the value of the currentStation property.

Add a constructor that creates a new instance of a CarRadio that is turned off and has the station set to 88.1.

Add a function called testCarRadio that instantiates a new car radio object, then calls the getStation method and returns the current station number.
