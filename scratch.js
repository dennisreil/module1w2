

const word1 = 'icecream';
const word2 = 'pizza';

console.log(word1.length + word2.length);

let string = "string  with  double  spaces";
console.log(string);
let fixed_string = string.replaceAll('  ', ' ');
console.log(fixed_string);


let firstName = "Sally";
let lastName = "Jones";
let occupation = "electrician";
let hobby = "gaming";
const sentence = `${firstName} ${lastName}, is an accomplished ${occupation} that enjoys ${hobby} in their spare time.`
// Add your code here
console.log(sentence);
// Sally Jones, is an accomplished electrician that enjoys gaming in their spare time.

let a = 5, b = 10, c = 20;
let result = ((a + b) * c);
console.log(result);

//class activity w2d1
const student1_grade1 = 80;
const student1_grade2 = 75;
const student1_grade3 = 60;
const student2_grade1 = 75;
const student2_grade2 = 92;
const student2_grade3 = 50;

const averageStudentGrades = ((((student1_grade1 + student1_grade2 + student1_grade3) /  3) + ((student2_grade1 + student2_grade2 + student2_grade3) / 3)) / 2);

const num = 157;
const str = "Hello World";
const bool = true;
const obj = {
    student: "dennis Reil",
    age: 29,
    occupation: "Coffee Roaster",
    graduate: false,
}

console.log(str, num, bool, obj);


//equality operator
const eqOpResult = 10 === 10;
console.log(eqOpResult);
console.log(1 === 1);
//greater than or equal to
console.log(2 >= 3);

//greater than or equal to
console.log(5 >= 3);

// NOT equal to
console.log(46578 !== 6578);

// and operator
console.log(1 === 1 && 2 === 2);

//or operator
console.log(1 !== 1 || 2 === 2);
