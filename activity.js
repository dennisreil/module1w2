// Create an object with a value and a circular reference to itself.
const original = { name: "MDN" };
original.itself = original;

// Clone it
const clone = structuredClone(original);

console.assert(clone !== original); // the objects are not the same (not same identity)
console.assert(clone.name === "MDN"); // they do have the same values
console.assert(clone.itself === clone); // and the circular reference is preserved
console.log(original);
console.log(clone);


const person = {
    name: "Dennis",
    age: 29,
}

const copy = structuredClone(person);
console.log(copy);






const letter = {a: 1, b:2}
const color = {c: 17, d: 18}

const newColor = Object.assign({}, color);

console.log(newColor);
//creates a CLONE. objects are NOT the same. Theyre different values stored in memory

// const letter = {a: 1, b:2}
// const color = {c: 17, d: 18}

// const newColor = Object.assign(color);

// console.log(newColor);
// //creates a CLONE. objects are grabbing the same data values
