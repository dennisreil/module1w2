function fizzbuzz(num) {
    // The word "fizzbuzz" if number is evenly divisible by both 3 and 5
    if (num % 3 === 0 && num % 5 === 0) {
        return "fizzbuzz";
        // The word "fizz" if number is evenly divisible by only 3
    }
    else if (num % 3 === 0) {
        return "fizz";
        // The word "buzz" if number is evenly divisible by only 5
    }
    else if (num % 5 === 0) {
        return "buzz";
        // The number if it is not evenly divisible by 3 nor 5
    }
    else {
        return num;
    }
}
console.log(fizzbuzz(3));
console.log(fizzbuzz(10));
console.log(fizzbuzz(9));
console.log(fizzbuzz(2));
console.log(fizzbuzz(30));
console.log(fizzbuzz(7));
export {};
//# sourceMappingURL=index.js.map