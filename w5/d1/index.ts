// Define the Shape type
type InputShapeType = 'circle' | 'square' | 'triangle';



// Define the IdentifyPolygonType type
type IdentifiedPolygonType = string | number;


// Create the identifyPolygons function

function identifyPolygons(inputShape: InputShapeType): IdentifiedPolygonType {
    if(inputShape === 'circle') {
        return 'Invalid polygon! That shape has no sides';
    }  if (inputShape === 'square'){
        return 4;
    }  if (inputShape === 'triangle') {
        return 3;
    }
    throw Error("no polygon");
    }

console.log(identifyPolygons('circle'));
console.log(identifyPolygons('square'));
console.log(identifyPolygons('triangle'));
