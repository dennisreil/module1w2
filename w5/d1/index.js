"use strict";
// Create the identifyPolygons function
function identifyPolygons(inputShape) {
    if (inputShape === "circle") {
        return 'Invalid polygon! That shape has no sides';
    }
    else if (inputShape === 'square') {
        return 4;
    }
    else if (inputShape === 'triangle') {
        return 3;
    }
    else {
        return 'Invalid polygon! That shape has no sides';
    }
}
console.log(identifyPolygons('circle'));
console.log(identifyPolygons('square'));
console.log(identifyPolygons('triangle'));
