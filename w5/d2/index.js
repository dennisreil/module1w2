function sum(a, b) {
    var result = a + b;
    return result;
}
function subtract(a, b) {
    var result = a - b;
    return result;
}
function multiply(a, b) {
    var result = a * b;
    return result;
}
function divide(a, b) {
    var result = a / b;
    return result;
}
function performOperation(fn, num1, num2) {
    return fn(num1, num2);
}
var resultSum = performOperation(sum, 5, 3);
console.log("Sum:", resultSum); // Output: Sum : 8
var resultMultiply = performOperation(multiply, 5, 3);
console.log("Multiply:", resultMultiply); // Output: Multiply: 15
var divided = performOperation(divide, 5, 3);
console.log("Multiply:", divided); // Output: Multiply: 15
