

type OperationFunction = (a: number, b: number) =>  number;


function sum(a: number, b: number): number {
    let result =  a + b
    return result;
}

function subtract(a: number, b: number): number {
    let result =  a - b
    return result;
}

function multiply(a: number, b: number): number {
    let result =  a * b
    return result;
}

function divide(a: number, b: number): number{
    let result =  a / b
    return result;
}

function performOperation(fn: OperationFunction, num1: number, num2: number): number {
     return fn(num1, num2);
}

const resultSum = performOperation(sum, 5, 3);
console.log("Sum:", resultSum); // Output: Sum : 8

const resultMultiply = performOperation(multiply, 5, 3);
console.log("Multiply:", resultMultiply); // Output: Multiply: 15

const divided = performOperation(divide, 5, 3);
console.log("Multiply:", divided); // Output: Multiply: 15
