let count = 10;

while (count >= 0) {
    console.log(count);
    count--;
}


let inc = 0;

while (inc <= 10) {
    console.log(inc);
    inc++;
}
// best for not knowing the number of iterations



// when you know the iteration amount in reference to ARRAY indexing

// to find index number
for (let i = 0; i <= 5; i++) {
    console.log(i);
}

for (let i = 10; i >= 0; i--) {
    console.log(i);
}


let guests = ["matt", "Erica", "Jon"];
for (let i= 0;  i < guests.length; i++) {
    console.log(guests[i]);
}


// for of loop
// iterable objects || to go through lists
let numbers = [1,2,3,4,5];

for (const n of numbers) {
    console.log(n);
}


//for in loops
// for objects
