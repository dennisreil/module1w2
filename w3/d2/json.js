//conversion of objects to JSON
const alice = { name: 'Alice', age: 25 };
const jsonString = JSON.stringify(alice);
console.log(jsonString); // ==> '{"name":"Alice","age":25}' 
console.log(alice.name);

//conversion of JSON to objects
const person = JSON.parse(jsonString) // ==> { name: 'Alice', age: 25 }
console.log(person); // ==> { name: 'Alice', age: 25 }
console.log(person.name); // ==> 'Alice'


const anObjectLiteral =  {

    aFunctionType: function(){return 'a'},

    anUndefinedType: undefined,

    anArrayWithAFunction: [ function (){ return 'c'; }]
}



console.log(anObjectLiteral);
const stringIfy = JSON.stringify(anObjectLiteral);
console.log(anObjectLiteral);
