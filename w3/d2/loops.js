let count = 1; // starts the count at 1

while (count <= 17) {
  console.log(count);
  count += 1;
}

console.log('the final value of count is', count);


let count1000 = 1000
while(count1000 <= 5) { // this is already false
  console.log(count1000); // this code is never run
}



let countDo = 1
do {
  console.log(countDo + " While loop with Do"); // this runs at least once no matter what
  countDo++; // increment count
} while (countDo <= 5) // conditional expression



let count1 = 1
while (count1 <= 5) { // conditional expression
  console.log(count1 +  " while loop without do"); // this only runs if the conditional evaluated to `true`
  count1++; // increment count
}




let breakLoop = 1;

// we're never incrementing `breakLoop`, but we'll exit the loop another way
while (breakLoop <= 5) {
  console.log(breakLoop); // prints 1
//  break;
// it's okay, `break` will exit out of the loop right away
}

console.log(breakLoop); // 1 this line is executed after we "break" out of the loop
