
let count = 1;
//heres a number, 1
// if 1 is less or equal to 10...
while (count <= 10) {
    // print the number
     console.log(count);
     //increment number by 1
     count += 1;
     //repeat
}


let counted = 10;

while (counted >= 1) {
     console.log("The count is: ", counted);
     counted -= 1;
}


let count2 = 2;

while (count2 <= 20) {
console.log(count2);
count2 += 2;
}




let countx2 = 1;

while (countx2 <= 5) {
console.log(countx2 * 2);
countx2 += 1;
}



//for loops
for (let count = 1; count <= 10; count++) {
    console.log("For loop in action: "+ count);
  }


const fruits = ["apple", "banana", "orange", "grape"];

for (let i = 0; i < fruits.length; i++) {
  let fruit = fruits[i]; // use the value of the counter variable
  console.log(`The fruit at array position ${i} is ${fruit}`);
}



let sum = 0;
    for (let i = 1; i <= 20; i++) {
        sum += i;
    }
    console.log(sum);


const fruitsLoop = ["apple", "banana", "orange", "grape"];
    for (let fruit of fruitsLoop) {
        console.log(fruit); // outputs: "apple" "banana" "orange" "grape"
    }
