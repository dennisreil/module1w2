let nestedObj = '{"name": "John", "age": 30, "city": "New York", "details": {"hobbies": ["reading", "coding", "gaming"], "education": {"highSchool": "XYZ High School", "university": "ABC University", "secretKey": "oldValue"}}}';



nestedObj = JSON.parse(nestedObj);
console.log(nestedObj);




nestedObj.details.education.secretKey = "I found it!";
console.log(nestedObj.details.education.secretKey);



console.log(nestedObj);



nestedObj = JSON.stringify(nestedObj);
console.log(nestedObj);



// Given the object treasureMap, access the value stored
//at the key buriedTreasure and store it in the variable treasure.
const treasureMap = {
    island: {
        cave: {
            buriedTreasure: "Gold bars!",
        },
    },
};
console.log(treasureMap);
let treasure = treasureMap.island.cave.buriedTreasure;
console.log(treasure);





let potions = {
    redPotion: {
        ingredients: {
            "Eye of Newt": 1,
            "Dragon Scale": 3,
        },
    },
};

potions.redPotion.ingredients["Dragon Scale"] = 4;
console.log(potions);




const galaxy = {
    stars: [
        { name: "Polaris", color: "yellow" },
        { name: "Betelgeuse", color: "red" },
        { name: "Vega", color: "blue" },
        { name: "Sirius", color: "white" },
    ],
};
let starColor = galaxy.stars[1].color;
console.log(starColor);




let game = {
    levels: [
        { id: 1, highScore: 1000 },
        { id: 2, highScore: 500 },
    ],
};

game.levels[0].highScore = 1200;
console.log(game);




const iceCreamFlavors = {
    customers: [
        {
            name: "John",
            age: 25,
            favoriteIceCream: ["Vanilla", "Mint Chip"],
        },
        {
            name: "Mary",
            age: 30,
            favoriteIceCream: ["Chocolate", "Cookies and Cream"],
        },
        {
            name: "Bob",
            age: 28,
            favoriteIceCream: ["Strawberry", "Rocky Road"],
        },
    ],
};

customerFavorites = JSON.stringify(iceCreamFlavors);
console.log(customerFavorites);






const rockets = {
    falcon: {
        fuel: {
            oxygen: 500,
            kerosene: 200,
        },
    },
};

rockets.falcon.fuel.oxygen = 600;
console.log(rockets);
const updatedRockets = JSON.stringify(rockets);
console.log(updatedRockets);




let basket = [
    ["Fruits",
        ["Apples", "Bananas", "Oranges"],
        [5, 3, 7]],
    ["Vegetables",
        ["Carrots", "Squash"],
        [2, 4]
    ],
];
let carrotQuantity = basket[1][2][0];
console.log(carrotQuantity)
console.log(basket[1][2][0]);


basket[1][1].push("Broccoli");
basket[1][2].push(6);

basket[1][2][0] = 10;
console.log(basket);
console.log(basket[1]);





const userDetails = `{
    "name": "John",
    "email": "johnSurfs@email.com",
    "age": 30,
    "username": "john123",
    "password": "secretPass456",
    "profession": "Software Engineer",
    "address": {
        "city": "Buffalo",
        "state": "New York",
        "zipCode": 98765
    },
    "preferences": {
        "theme": "light",
        "language": "en-US"
    },
    "hobbies": ["surfing", "hiking", "photography"],
    "friends": [
        {
        "name": "Alice",
        "age": 28,
        "email": "alice@email.com"
        },
        {
        "name": "Bob",
        "age": 32,
        "email": "bob@email.com"
        }
    ]
    }`;

let user = JSON.parse(userDetails);
console.log(user);

user.address.zipCode = 14201;
user.preferences.theme = "dark";

 console.log(user);

 let updatedUserDetails = user;
 updatedUserDetails = JSON.stringify(updatedUserDetails);



let bestChocolateCakeRecipe = {
    recipe: {
        name: "World's Best Chocolate Cake",
        difficulty: "Advanced",
        servings: 12,
        ingredients: [
            { name: "high-quality dark chocolate", quantity: "8 oz" },
            { name: "unsalted butter", quantity: "1 cup" },
            { name: "granulated sugar", quantity: "1 cup" },
            { name: "large eggs", quantity: "4" },
            { name: "all-purpose flour", quantity: "1 1/2 cups" },
            { name: "baking powder", quantity: "1 teaspoon" },
            { name: "bananas", quantity: "3 cups" },
            { name: "whole milk", quantity: "1 cup" },
            { name: "vanilla extract", quantity: "2 teaspoons" },
        ],
        instructions: [
            "Preheat the oven to 350°F (175°C). Grease and line two 9-inch round cake pans.",
            "In a heatproof bowl, melt the dark chocolate and butter together over a pan of simmering water. Stir until smooth, then remove from heat and let it cool slightly.",
            "In a separate bowl, whisk together sugar and eggs until pale and creamy.",
            "Add the melted chocolate mixture to the egg mixture and whisk until well combined.",
            "Sift in the flour, baking powder, and salt. Fold gently until just combined.",
            "Gradually add milk and vanilla extract while stirring, until the batter is smooth.",
            "Divide the batter equally between the prepared cake pans.",
            "Bake for 25-30 minutes or until a toothpick inserted into the center comes out clean.",
            "Remove from the oven and let the cakes cool in the pans for 10 minutes. Then transfer them to a wire rack to cool completely.",
            "Once cooled, frost the cakes with your favorite chocolate ganache or buttercream frosting.",
            "Serve and enjoy the world's best chocolate cake!",
        ],
        totalTime: "2 hours",
        rating: "⭐️⭐️⭐️⭐️⭐️",
    },
};

console.log(bestChocolateCakeRecipe.recipe.ingredients[6]);

bestChocolateCakeRecipe.recipe.ingredients[6].name = "salt";
bestChocolateCakeRecipe.recipe.ingredients[6].quantity = '1/4 teaspoon';
console.log(bestChocolateCakeRecipe.recipe.ingredients);

updatedRecipe = JSON.stringify(bestChocolateCakeRecipe);
