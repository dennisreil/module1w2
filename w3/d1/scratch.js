const galacticDatabaseJSON = `{
    "name": "Milky Way",
    "age": "13.51 billion years",
    "stars": [
        {
            "name": "Polaris",
            "color": "yellow",
            "distanceFromEarth": "433.8 light-years",
            "planets": [
                {
                    "name": "Polaris I",
                    "type": "Gas Giant"
                },
                {
                    "name": "Polaris II",
                    "type": "Terrestrial"
                }
            ]
        },
        {
            "name": "Betelgeuse",
            "color": "red",
            "distanceFromEarth": "500 light-years",
            "planets": []
        }
    ],
    "blackHoles": [
        {
            "name": "Sagittarius A*",
            "mass": "4.31 million solar masses"
        },
        {
            "name": "Cygnus X-1",
            "mass": "14.8 solar masses"
        }
    ],
    "clusters": [
        {
            "name": "Orion Nebula",
            "stars": [
                {
                    "name": "Orion I",
                    "color": "blue"
                },
                {
                    "name": "Orion II",
                    "color": "white"
                },
                {
                    "name": "Orion III",
                    "color": "yellow"
                }
            ]
        },
        {
            "name": "AndromedaCluster",
            "stars": [
                {
                    "name": "Andromeda I",
                    "color": "red"
                },
                {
                    "name": "Andromeda II",
                    "color": "orange"
                },
                {
                    "name": "Andromeda III",
                    "color": "blue"
                }
            ]
        }
    ]
}`;


galacticDatabase = JSON.parse(galacticDatabaseJSON);
console.log(galacticDatabase);


let firstStarName = galacticDatabase.stars[0].name;
console.log(firstStarName);

galacticDatabase.stars[1].distanceFromEarth = "642.5 light-years";
console.log(galacticDatabase.stars[1].distanceFromEarth);


// Star: Sirius
// color Blue,
//distance 8.6 light-years
// planets [],

galacticDatabase.stars.push({name: "Sirius",color: "blue", distanceFromEarth: "8.6 light-years", planets: []});
console.log(galacticDatabase);


updatedGalacticDatabaseJSON = JSON.stringify(galacticDatabase);
