const person = {
  name: "Alice",
  age: 25,
  address: {
    street: "123 Main St",
    city: "Los Angeles",
    country: "USA",
  },
};
console.log(person.address);
/*
while dot notation is a way of selected properties
within an object thats nested, you can also use brackets
*/

//setting the property in a nested object
person["address"]["state"] = "CA";
// updating the property in a nested object
person.address.state = "California";
// accessing the property in a nested object
console.log(person["address"]["state"]);
console.log(person["name"], person.address.city);

// since address hasn't been initialized, city wont be either.
// to resolve this, a nested object must be made with address. THEN initialize city
const person2 = { name: "Zahara", age: 21 };
//person2.address2.city2 = "San Diego";
console.log(person2);
//heres how to resolve this,

const alice1 = {
  name: "Alice",
  age: 25,
};
alice1.address = {};
alice1.address.city = "Los Angeles";
console.log("Alice 1 ", alice1.address.city); // ==> "Los Angeles"

const alice2 = {
  name: "Alice",
  age: 25,
};
alice2.address = {
  city: "Los Angeles",
};
console.log("Alice2 ", alice2.address.city);

//if Else is useful to check if key exists before creating or adding value
const ifElseObj = {
  name: "Alice",
  age: 25,
};
if (ifElseObj && ifElseObj.address3 !== undefined) {
  ifElseObj.address3.city3 = "Los Angeles";
  console.log("City successfully updated to ", ifElseObj.address.city);
} else {
  console.log(
    "If Else verification of key values. person.address is undefined"
  );
}

const person3 = {
  name: "Alice",
  age: 25,
  address: {
    street: {
      name: "Main Street",
      number: "123",
    },
    city: "New York",
    country: "USA",
    zipCode: "90210",
  },
};

console.log(person3.address.street.name);
console.log(person3["address"]["street"]["name"]);
