// Importing members from the math module
import { square, PI }
from './math.js';

console.log(square(5));
// Output: 25
console.log(PI);
// Output: 3.14159


// main.js
import myFunc from
'./module.js';

console.log(myFunc());
