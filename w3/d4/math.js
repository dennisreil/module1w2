
// Exporting a function
export function square(x) {
    return x * x;
  }


  // Exporting a constant
  export const PI = 3.14159;
