function isOldEnoughToDrink(age) {
    if(age >= 21) {
        return true;
    } else {
        return false;
    }
    // your code here
  }
//   console.log(isOldEnoughToDrink(18));

  const vowels = ['a', 'e', 'i', 'o', 'u'];

function countVowels(word) {
    let count = 0;
    for (let letter of word) {
        if (vowels.includes(letter)) {
            count ++;
        }
    }
    return count;
}
//   console.log(countVowels("dog"));


// We are given a "pizza" object with a "toppings" key whose value is an array of toppings!
// Write a function that takes the pizza and a new topping and returns a new list of toppings containing both the new topping and the toppings from the pizza object.
// Then call that function with the pizza and "pineapple". Take the returned toppings array and update the pizza with the new array.




  const pizza = {
    'toppings': ['cheese', 'peppers', 'sausage']
  };
  function addTopping(pizzaParameter, topping) {
    // initialize a variable that IS the array of the first parameter
    let newTopping = pizzaParameter.toppings;
    // Add the value of the second parameter INTO the first parameter
    newTopping.push(topping);
    // close out the function
    return newTopping;
  }
//   console.log(addTopping(pizza, "pineapple"));
//   console.log(pizza.toppings);

  // Call the addTopping function here and add "pineapple" to it.
  // and then modify the original pizza to have the new list of toppings.


  var obj = {
    name: 'Sam',
    age: 20
  }



  function removeProperty(obj, key) {
    for (let value in obj) {{
            delete obj[value];
        }
    }
return obj;
}


removeProperty(obj, 'name');
console.log(obj.name);






    function isSameLength(word1, word2) {
    if (word1.length === word2.length){
  return true;
    } else {
        return false;
    }
}

console.log(isSameLength("no", "noo"));


function areBothOdd(num1, num2) {

    let newNum = [];
    newNum.push(num1, num2);
   let total = num1 + num2;
   let average = total / newNum.length;
console.log(average);
return average;
}
console.log(areBothOdd(8, 8));



function joinArrays(arr1, arr2) {

        let newArray = arr1.concat(arr2);
        return newArray;
  }

  console.log(joinArrays([1,2,3], [48,500]))



function getNthElement(array, n) {

if (array.length > 0){

    return array[n];
} else {
    return undefined;
}
}



const value = getNthElement([], 0);
console.log(value);



// return the index of the place the character exists in the string
function getIndexOf(char, str) {

    let index = -1;
    for (i = 0; i < str.length -1; i++) {
        if (char === str[i]) {
    return i;
        }
        }
    return index;
    }
const letter = getIndexOf('h', 'I am a hacker');
console.log(letter);



const arrs = [1,2,3,4,5];

function removeFromBack(array) {
    array.pop();
    return array;
}

console.log(removeFromBack(arrs));




// function select(arr, obj) {
//   //if obj key === arr[i], send key value pair to new object

// so far so good
let newObject = {};
  for (i = 0; i < arr.length - 1; i++) {
// loop through the object keys
    for (key in obj) {
//compare the key to the array value at index
      if (key === arr[i]) {
         newObject = Object.assign(newObject,{ [arr[i]]: obj[key] });
      }
    }
  return newObject;
 }
  // console.log(newObject)


const arr = ['a', 'c', 'e'];
const obj = {
  a: 1,
  b: 2,
  c: 3,
  d: 4
};
const output = select(arr, obj);
console.log(output);



const jsonString = `{
  "book": {
    "title": "The Chronicles of Fictionland",
    "author": "J.K. Novelist",
    "genre": "Fantasy",
    "year": 2022,
    "pages": 350,
    "publisher": "Fiction Publishing House",
    "metadata": {
      "rating": 4.5,
      "reviews": [
        {
          "username": "reader1",
          "rating": 4
        },
        {
          "username": "reader2",
          "rating": 5
        }
      ]
    }
  }
}`;

const propertyPath = "book.metadata.rating";

function getNestedPropertyValue(jsonString, propertyPath) {
let newObject = JSON.parse(jsonString);

// loop through the keys of the object
for (let bookInfo in newObject) {
  for (let metadata in newObject[bookInfo]) {
    if (propertyPath === newObject[bookInfo][metadata]) {
      }
  }
}
}


console.log(propertyPath);
console.log(getNestedPropertyValue(jsonString, propertyPath));






function swapFirstAndLast(arr) {
// if value at array = looped value
for (let value in arr) {
    array  = arr[value];
    let [a, z] = arr;
    let newArr = [];
    newArr.push(a);

    newArr.unshift(z);
    console.log(newArr);
    return newArr;
}

    const x = [1, 2, 3, 4, 5];
    const [y, z] = x;
    // console.log(y); // 1
    // console.log(z); // 2
}
    const arrIn = ['pineapple', 'orange']
console.log(swapFirstAndLast(arrIn));
