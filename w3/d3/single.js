

function calculateAverage(arr) {
  // create a function with a parameter called array
  // within the function an expression is made that will be passed into the parameter arr

  // to find average, we first need a number to average
  //if statement will look and see if there are any numbers at all
  if (arr.length === 0) {
    // if there are no number, return the error
    throw new Error("Array cannot be empty.");
  }
  // we need a variable that will be a total of all the values in the array
  // so we initialize with a value of 0 (because we haven't started adding anything yet.)
  let totalSum = 0;
  // we will use the for of loop which will go through each array index
  // we create a variable called num which will be the index value for the array values
  for (const num of arr) {
    // we then take the current value of totalSum variable (ORIGINALLY 0) then add the index value of num.
    totalSum += num;
    // the for of loop will go through each iterable item in the array (each array index) until theres no more
    // meaning the totalSum += num will update totalSum to it's current value plus the iterable value of the arr, until each arr iterable has been gone over.
    //our total sum is now all the array iterables added together.
  }

  // we have a sum, now we need an average

  // we take the total sum ad divide it by the array length. (how many iterables, values, numbers do we have)
  // this gets passed along to a new variable called average

  const average = totalSum / arr.length;
  // then to ensure this function is usable, we return the final result of our expression (average) which gets passed into the array parameter
  return average;
  // whatever array we put into the function going forward, or variable that has an array; this expression will run the average every time.
}
let array = [1, 2, 3, 4, 5];
  console.log(calculateAverage(array)); // Expected output: 3
  console.log(calculateAverage([10, 20, 30, 40, 50])); // Expected output: 30
  console.log(calculateAverage([-1, 0, 1]));
