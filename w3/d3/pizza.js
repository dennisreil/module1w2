function calculatePizzaOrderTotal(
    numberOfPizzas,
    pizzaSize,
    extraCheese,
    topping,
    numberOfDrinks,
    drinkType,
    delivery
) {
    let price = 0;

    if (numberOfPizzas) {
        if (pizzaSize === "small") {
            price += 10 * numberOfPizzas;
        } else if (pizzaSize === "medium") {
            price += 12 * numberOfPizzas;
        } else if (pizzaSize === "large") {
            price += 15 * numberOfPizzas;
        }
        if (extraCheese) {
            price += 2 * numberOfPizzas;
        }
        if (topping) {
            if (topping === "pepperoni") {
                price += 1 * numberOfPizzas;
            } else if (topping === "mushrooms") {
                price += 1.5 * numberOfPizzas;
            } else if (topping === "sausage") {
                price += 2 * numberOfPizzas;
            }
        }
    }

    if (numberOfDrinks) {
        if (drinkType === "soda") {
            price += 2 * numberOfDrinks;
        } else if (drinkType === "beer") {
            price += 5 * numberOfDrinks;
        }
    }

    if (delivery) {
        price += 5;
    }

    return price;
}

























function numberOfPizzas(pizzaSize) {
 let totalPrice = 0;

for (let i = 0; i < pizzaSize.length; i++) {
    const pizza = pizzaSize[i];
    let pizzaTotal = 0;
// iterate each item available
    if (pizza.quantity > 0 && pizza.price > 0) {
        // if item has stock, multiply item by price
        pizzaTotal = pizza.quantity * pizza.price;
    }
// total price is item total
    totalPrice += pizzaTotal;
}

return totalPrice;


if (small === "small") {
    price * 10 + totalPrice;
} else if (medium === "medium") {
    price += 12 * totalPrice;
} else if (large === "large") {
    price += 15 * totalPrice;
}
return totalPrice;






}
console.log("number of pizzas price: " + numberOfPizzas(3,5,6));


function extraCheese() {
if (extraCheese) {
    price += 2 * numberOfPizzas;
}
if (topping) {
    if (topping === "pepperoni") {
        price += 1 * numberOfPizzas;
    } else if (topping === "mushrooms") {
        price += 1.5 * numberOfPizzas;
    } else if (topping === "sausage") {
        price += 2 * numberOfPizzas;
    }
}
}
