let my_order = {
  items: [
      { quantity: 4, price: 5 }
    ],
  discount: 10,
  shipping: true,
};



function calculateOrderTotal(order) {
  let totalPrice = 0;

  for (let item of order.items) {
    let itemTotal = 0;

    if (item.quantity && item.price) {
      itemTotal = item.quantity * item.price;
    }
    totalPrice += itemTotal;
  }
  return totalPrice;
}


function applyDiscount(order, totalPrice) {
  if (order.discount > 0) {
    console.log(totalPrice);
    let discountAmount = (order.discount / 100) * totalPrice;
    totalPrice -= discountAmount;
  }
  return totalPrice;
}

function addTax(totalPrice) {
  const taxRate = 0.1;
  const taxAmount = taxRate * totalPrice;
  totalPrice += taxAmount;
  return totalPrice;
}

function addShipping(order, totalPrice) {
  if (order.shipping) {
    totalPrice += 5;
    }
  return totalPrice;
}



function handleOrderProcessing(order) {
  let totalPrice = calculateOrderTotal(order);
  totalPrice = applyDiscount(order, totalPrice);
  totalPrice = addTax(totalPrice);
  totalPrice = addShipping(order, totalPrice);
  return totalPrice;
}

console.log(handleOrderProcessing(my_order));
