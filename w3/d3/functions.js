let name = "Dennis";
function greetGuest(name) {
  console.log("Hello, " + name + "!");
}
name = "jonathan";
console.log(greetGuest(name));

function greetUser(name) {
  const message = "Hello, " + name + "! It's time for functions!!";
  console.log(message);
}
greetUser("Alice");
greetUser("Bob");
greetUser("Charlie");

function multiplyNumbers(a, b) {
  const result = a * b;
  return result;
}
console.log(multiplyNumbers(5, 3)); // Output: 15
console.log(multiplyNumbers(2, 4)); // Output: 8
console.log(multiplyNumbers(10, -2)); // Output: -20

function addTwoNumbers(a, b) {
  return a + b;
}
console.log(addTwoNumbers(99, 98));

function subtractTwoNumbers(second, first) {
  return second - first;
}

function subtractTwoNumbers(first, second) {
  return first - second;
}
let currentYear = 2023;
let birthYear = 1989;
let age = subtractTwoNumbers(currentYear, birthYear);
console.log(age);




function processOrder(items, discount, shipping) {
  let totalPrice = 0;

  // Calculate total price of items
  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    let itemTotal = 0;

    if (item.quantity > 0 && item.price > 0) {
      itemTotal = item.quantity * item.price;
    }

    totalPrice += itemTotal;
  }
  // Apply discount to total price
  if (discount > 0) {
    const discountAmount = (discount / 100) * totalPrice;
    totalPrice -= discountAmount;
  }

  // Add tax to total price
  const taxRate = 0.1;
  const taxAmount = taxRate * totalPrice;
  totalPrice += taxAmount;

  // Add shipping costs to total price
  if (shipping) {
    totalPrice += 5;
  }

  return totalPrice;
}

const orderOne = [{ quantity: 1, price: 10 }]; // stores an array of objects
const discountOne = 0; // stores a number
const shippingOne = false; // stores a boolean value

console.log(processOrder(orderOne, discountOne, shippingOne));

// Output: 74.3
const orderTwo = [
  { quantity: 3, price: 20 },
  { quantity: 2, price: 5 },
]; // stores an array of objects
const discountTwo = 10; // stores a number
const shippingTwo = true; // stores a boolean value

console.log(processOrder(orderTwo, discountTwo, shippingTwo));
