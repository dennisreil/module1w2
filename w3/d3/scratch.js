


function calculateTotalPrice(items) {
// nothings been ordered yet. shopping cart value is always 0
    let totalPrice = 0;

    for (let i = 0; i < items.length; i++) {
        const item = items[i];
        let itemTotal = 0;
// iterate each item available
        if (item.quantity > 0 && item.price > 0) {
            // if item has stock, multiply item by price
            itemTotal = item.quantity * item.price;
        }
// total price is item total
        totalPrice += itemTotal;
    }

    return totalPrice;
}



// Apply discount to total price
function applyDiscount(totalPrice, discount) {
// if discount exists (value is more than 0)
    if (discount > 0) {
        const discountAmount = (discount / 100) * totalPrice;
// add discount to price by subtracting value of price * discount
        totalPrice -= discountAmount;
    }
// update price
    return totalPrice;
}


// Add tax to total price
function addTax(totalPrice) {
    const taxRate = 0.1;
    const taxAmount = taxRate * totalPrice;
    totalPrice += taxAmount;

    return totalPrice;
}

// add shipping after tax and discount
function addShipping(totalPrice, shipping) {
    if (shipping) {
        totalPrice += 5;
    }

    return totalPrice;
}




// Reassemble the smaller functions into a new version of the `processOrder` function
function processOrder(items, discount, shipping) {
    // Step 1: Calculate total price of items
    const totalPrice = calculateTotalPrice(items);
    // Step 2: Apply discount to total price
    const totalPriceAfterDiscount = applyDiscount(totalPrice, discount);
    // Step 3: Add tax to total price
    const totalPriceWithTax = addTax(totalPriceAfterDiscount);
    // Step 4: Add shipping costs to total price
    const finalPrice = addShipping(totalPriceWithTax, shipping);

    return finalPrice;
}
