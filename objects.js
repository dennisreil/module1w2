



const user = {
    name: "Dennis",
    age: 29,
    jobs: ["Panda Express","Pest Control", "Coffee Roaster"],
    city: "Bellevue"
}

console.log(user);
console.log(user.jobs[1]);
console.log(user["jobs"][2]);

user["name"] = "Joel";
console.log(user.name);

user["jobs"][1] = "Construction";
console.log(user.jobs);

