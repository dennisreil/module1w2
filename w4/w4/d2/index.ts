// /*
// It will help to copy this to your code editor.
// Then, paste your solution back here to submit your work for grading.

// First work on defining types for an Order and an Item.
// Then add type parameters where needed.

// If you have time, also work on breaking down this large function
// into a main function, and smaller helper functions.
// */

// function handleOrderProcessing(order) {
//     let totalPrice = 0;

//     for (let i = 0; i < order.items.length; i++) {
//       const item = order.items[i];
//       let itemTotal = 0;

//       if (item.quantity > 0 && item.price > 0) {
//         itemTotal = item.quantity * item.price;
//       }

//       totalPrice += itemTotal;
//     }

//     if (order.discount > 0) {
//       const discountAmount = (order.discount / 100) * totalPrice;
//       totalPrice -= discountAmount;
//     }

//     const taxRate = 0.1;
//     const taxAmount = taxRate * totalPrice;
//     totalPrice += taxAmount;

//     if (order.shipping) {
//       totalPrice += 5;
//     }

//     return totalPrice;
//   }

 let new_order = {
    "items": [{ "quantity": 10, "price": 15 }],
    "discount": 10,
    "shipping": true
}

// Create an interface, or type alias, for "Order" and "Item".

// Rewrite the "handleOrderProcessing" function with types


  // If you have time, define these helper functions, and
  // refactor your main function to use them
  // Think through your parameters and return values.


type Order = {
 items: Item[],
discount: number,
shipping: boolean
}


type Item = {
  quantity: number,
  price: number
}


  function calculateItemTotal(order: Order) {
     let totalPrice = 0;

    for (let i = 0; i < order.items.length; i++) {
      const item = order.items[i];
      let itemTotal = 0;

      if (item.quantity > 0 && item.price > 0) {
        itemTotal = item.quantity * item.price;
      }

      totalPrice += itemTotal;
    }
return totalPrice;

}

  function applyDiscount(order: Order, totalPrice: number){
    if (order.discount > 0) {
        const discountAmount = (order.discount / 100) * totalPrice;
        totalPrice -= discountAmount;
        }
    return totalPrice;
  }

  function calculateTax(totalPrice: number){
    const taxRate = 0.1;
    const taxAmount = taxRate * totalPrice;
    totalPrice += taxAmount;
    return totalPrice;
  }

  function addShipping(order: Order, totalPrice: number){
    if (order.shipping) {
        totalPrice += 5;
      }
      return totalPrice;
  }


  function main(order: Order) {
   let totalPrice = calculateItemTotal(order);
    totalPrice = applyDiscount(order, totalPrice);
    totalPrice = calculateTax(totalPrice);
    totalPrice = addShipping(order, totalPrice);
    return totalPrice;
  }

main(new_order);
