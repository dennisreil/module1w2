import { NumericLiteral } from "./node_modules/typescript/lib/typescript";

function printNameAndAge(name: string, age: number, message: string) {
    console.log(name, "is", age, "years old", message);
  }

  function hasShortName(name: string): boolean {
    return name.length <= 4;
  }

  const names: string[] = ["Asa", "Baz", "Caris", "Duska"];
  const ages: number[] = [22, 31, 29, 44];

  for (let i: number = 0; i < names.length; i += 1) {
    const name: string = names[i];
    const age: number = ages[i];
    if (hasShortName(name)) {
      printNameAndAge(name, age, "and has a short name");
    } else {
      printNameAndAge(name, age, "");
    }
  }


  function addTwoNumbers(x: number, y: number) {
    return x + y;
  }

  const result = addTwoNumbers(3, 5);
