function printNameAndAge(name, age, message) {
    console.log(name, "is", age, "years old", message);
}
function hasShortName(name) {
    return name.length <= 4;
}
const names = ["Asa", "Baz", "Caris", "Duska"];
const ages = [22, 31, 29, 44];
for (let i = 0; i < names.length; i += 1) {
    const name = names[i];
    const age = ages[i];
    if (hasShortName(name)) {
        printNameAndAge(name, age, "and has a short name");
    }
    else {
        printNameAndAge(name, age, "");
    }
}
function addTwoNumbers(x, y) {
    return x + y;
}
const result = addTwoNumbers(3, 5);




const rectangle = {
    width: 6,
    height: 8
   };
function getArea( area) {
   let result = area.width * area.height;
   return result;
}
function getPerimeter(perimeter) {
   let result = 2 *(perimeter.width + perimeter.height);
   return result;
}
console.log(getPerimeter(rectangle));
console.log(getArea(rectangle));
