let x = 10; // Variable x is declared outside the if block

if (true) {
  let x = 5; // Variable x is declared inside the if block
  console.log(x); // Output: 5
}

console.log(x);


let bankAccount = 'empty';

if (true){
    bankAccount = 'full';
    console.log(bankAccount);
}


let three = 3;
let five = 5;
console.log (three === five);
console.log (three < five);
console.log (three > five);


let string3 = "3";
let number3 = 3;

console.log(string3 === number3);
console.log(string3 == number3);
console.log(string3 !== number3);
console.log(string3 != number3);


// three ways to convert a string to a number
let str = "42";
let str_to_num = parseInt(str, 10); // number: 42
let str_to_num2 = Number(str); // number: 42
let str_to_num3 = +str; // number: 42 using the "unary plus operator"
console.log ("string to number coercion", str_to_num, str_to_num2, str_to_num3);

// two ways to convert a number to a string
let num = 42;
let num_to_str = num.toString(); // string: '42'
let num_to_str2 = String(num); // string: '42'
console.log ("number to string coercion", num, num_to_str, num_to_str2);



// convert a Boolean to a string and a number
let bool_true = true;
let bool_to_str = String(bool_true); // string: 'true'
let bool_true_to_num = Number(bool_true); // number: 1
let bool_false = false;
let bool_false_to_num = Number(bool_false); // number: 0
console.log ("boolean to number or string coercion:", bool_true = bool_to_str + ' ' + bool_true_to_num + ' ' + bool_false + ' ' + bool_false_to_num);



let xx = 5;
let yy = "10";
let result = xx + yy;
console.log(result);


console.log("45" - 4); // 41
console.log("33" - "11"); // 22
