try {
    const x = 10;
    const y = 3;
    const z = 15;
    let result = x / z;
    console.log(result);
  } catch (error) {
    console.log(error.message);
  }

  try {
    const x = 10;
    let result = x;
    console.log(result);
  } catch (error) {
    console.log(error.message);
  }


  try {
    const y = 3;
    let result = y;
    console.log(result);
  } catch (error) {
    console.log(error.message);
  }

  try {
    const x = 10;
    const y = 3;
    let result = y / x;
    console.log(result);
  } catch (error) {
    console.log(error.message);
  }


  try {
    const z = 15;
    let result = z;
    console.log(result);
  } catch (error) {
    console.log(error.message);
  }
