

/*
if (condition) {
    body
    what you want the code to do
}
*/

if (9 < 15) {
    console.log("9 is less than 15");
}
//if the statement is true, it'll console log
console.log("response after if then");

//if the statement is NOT true, it skips
if (20 < 15) {
    console.log("9 is less than 15");
}
console.log("response after if then");

//changing values in local scope while global remains
let a = 5;
let b = 20;
if (a < b) {
    b = 10;
    a = 200;
    console.log(`yes ${a} is less than ${b}`);
}
    console.log(`no ${a} is NOT less than ${b}`);


// else statement is your NOT statement
if (x > 5) {
    console.log("x is greater than 5");
} else {
    console.log("x is less or equal to 5");
}


let temperature = 25;

if (temperature < 0) {
    console.log("It's freezing!");
} else if (temperature < 10) {
    console.log("It's cold.");
} else if (temperature < 20) {
    console.log("It's cool.");
} else {
    console.log("It's warm.");
}


let age = 21;
let hasLicense = false;

if (age >= 18 && hasLicense) {
    console.log("freedom")
} else {
    console.log("study for drivers test")
}




let feeling = 'sad';

switch (feeling) {
  case 'happy':
      console.log("I'm glad you're feeling good!")
      break;
  case 'sad':
      // this is the only code that will be executed
      console.log("I'm sorry you're feeling sad!")
      break;
  case 'mad':
      console.log("I see you're feeling mad!")
      break;
  default:
      console.log("I see you're not having any strong feelings.")
      break;
}

let grade = "B"
switch(grade) {
    case "A" :
    console.log("Awesome");
        break;
    case "B":
    console.log("That's pretty good");
        break;
    case "C" :
    console.log("Passing but need to study");
        break;
    case "D" :
    console.log("not looking good");
        break;
    case "F" :
    console.log("failed");
        break;
    default:
    console.log("Grade not posted")
        break;
}


try {
    let userInput;
    if (formInput !== undefined) {
      userInput = formInput;
    }
    console.log(userInput);
  } catch (error) {
    console.log(error.message);
  }


//Ternary is when you want to assign a variable based on a condition
// because you cant assign a variable to an if else statement
  const ternaryResult = a < b
    ? "a is less than b"
    : "a is NOT less than b"


const number = 10;
const expression = number % 1 === true;
console.log(expression);


console.log(9 % 2);
