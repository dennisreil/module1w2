let x = 3;
let y = 4;
let z = x + y;

console.log("The value of z is " + z);

let a = (x + y) / z;
let b = x + y / z;
console.log("The value of a is " + a);
console.log("The value of b is " + b);

console.log("Hello" + 1, 2 + "World");

h = "Hello";
w = "World";

console.log(w.length);
//.length prints the numerical length of the variable selected
console.log(w[0]);
//[0] will print the value of the string at index 0 or the first value
console.log(w[w.length - 1]);
// [(variable_name).length - 1] will print the last index value of the variable selected.

let noun1 = "Dog";
let verb = "barks";
let noun2 = "Moon";
let dog_sentence = "The " + noun1 + " " + verb + " at the " + noun2;
console.log(dog_sentence);
// sentence contains "The Dog barks at the Moon";

// Concatenation
let noun_concat = "Cat";
let verb_concat = "meows";
let noun2_concat = "Sun";
let easyConcat = `The ${noun_concat} ${verb_concat} at the ${noun2_concat}`;

console.log(easyConcat);

// backtick ` will auto space string concatenation (but the variable called MUST be within the syntax ${variable_name} )

let favoriteFruit = "My favorite fruit is Watermelon";
console.log(favoriteFruit.slice(21));
console.log(favoriteFruit);

console.log(favoriteFruit.slice(5, 10));

const exclamation = "Developers! ";
console.log(exclamation.repeat(10));

const char = "********";
console.log(char.length);

let sampleString = "Hello World!";
console.log(sampleString.slice(6));

let sentence = "ThIs SeNTenCE NeeDs TO Be ALL LoWER CaSE";
console.log(sentence.toLocaleLowerCase());
