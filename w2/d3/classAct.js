//arrays are variables that hold multiple data properties

let names = [
    "Dennis", "Erica", "Lacy", "Dexter"
]
//arrays are identified by the square braces and begin with index 0
let groceryList = [
    "eggs", "milk", "bread", "cheese", "butter", "Ice Cream"
]
console.log(groceryList);

//here we reassign the value of index 1 to pizza
 groceryList[1] = "Pizza";
 console.log(groceryList);


 console.log(groceryList.length);

 /*
 this index math formula is taking the length of the array found at list,
 subtracting one then assigning that number as the index value of lastItem
*/
 const lastItem = groceryList[groceryList.length - 1];
console.log(lastItem);

//push add a value to the end
groceryList.push("mangos");
console.log(groceryList);

//pop will remove the last value in an array
groceryList.pop();
console.log(groceryList);


// will add value to front of array
groceryList.unshift("bananas") ;
console.log(groceryList);

//.shift() removes the value at beginning of the array

groceryList.shift() ;
console.log(groceryList);


let values = [100, 80, 200, 30, 49, 24];

let [num1, num2, ...others] = values;
console.log(num1);
console.log(others);
console.log(values);


//objects use curly brackets that have key-value pairs
//in this, the names are keys, values are the colors
favoriteColors = {
    dennis: "blue",
    john: "green",
    laura: "yellow"
}
console.log(favoriteColors.dennis);

//both ways access the value for dennis but when searching index/key,
//they are stored as strings
console.log(favoriteColors["dennis"]);


//adding key:value pairs
favoriteColors["Justin"] = "Purple";
favoriteColors.Bart = "White";

console.log(favoriteColors);
//deleting key:value pairs
delete favoriteColors["dennis"];
console.log(favoriteColors);


// this makes the assigns the values variables
const fruitDestruc = ["apple", "banana", "orange", "pineapple", "cherry"];
const [apple, banana, orange, pineapple, cherry] = fruitDestruc;

console.log(fruitDestruc);
console.log(apple);



// const output = [1, 2];
// const element = output.push(3);
// console.log(output)


const output = [1, 2]
const element = 3;

 output.unshift(element);
console.log(output);


//this will add a new key (full name) with the values of the keys firstName and lastName
const person = {
    firstName: 'Jade',
    lastName: 'Smith',

  };
person.fullName =`${person.firstName} ${person.lastName}`
  console.log(person)
