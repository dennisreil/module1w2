const fruits = ["apple", "banana", "orange", "pineapple", "cherry"];
    fruits[0] = "avocado";
        console.log(fruits[0]);
        console.log(fruits.length);

let lastElement = (fruits.length - 1);
    console.log(lastElement);


const fruitsArray = ["apple", "banana", "orange"];
    let len = fruitsArray.length; // 3
    let lastElem = fruitsArray[len - 1];
        console.log(lastElem);
//this method will make lastElement a variable with the value of the last index
    let thirdFromLast = fruits[fruits.length - 3];
        console.log("variable assigned to third from last index: ", thirdFromLast);


//split  breaks up the strings and fills in the space with what input into the ""
//empty "" with auto-space with a ,
let favoriteFruit = "My favorite fruit is Watermelon";
let newArray = favoriteFruit.split(" ");
console.log(newArray);


//join will merge the array into a string
let newString = newArray.join();
console.log(newString);
console.log(newArray.join('_'));

//add value to end of array
fruits.push("lemon");
console.log(fruits);

//remove value at beginning of array
fruits.unshift("Strawberry");
console.log(fruits);

//remove value at end of array
fruits.pop();
//remove value at beginning of array
fruits.shift();
console.log(fruits);


const colors = ["red","green","blue"];

const firstColor = colors[0];
const secondColor = colors[1];
const thirdColor = colors[2];
console.log(colors);

const [red, blue, green] = colors;
console.log(red);

//this is destructuring arrays
//it's initializing  variables to array values in the order to place them
const fruitDestruc = ["apple", "banana", "orange", "pineapple", "cherry"];
const [apple, banana, orange, ...anyName] = fruitDestruc;

console.log(fruitDestruc);
console.log(apple);
console.log(...anyName)
