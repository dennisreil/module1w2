const person = {
    name: "Alice",
    age: 25,
};
console.log(person);
console.log(person.age);
//this is the same way of creating an object as...

const newPerson = new Object();
person.name = "Alice"; //setting the value of the property
console.log(person.name); //accessing the value of the property
//this method allows the addition of new variable outside of the {} method
person.age = 25; //setting the property value
console.log(person.age); //accessing the property value
console.log(person);


person["name"] = "James"; //setting the property value
console.log(person["name"]); //accessing the property value

person["age"] = 30; //setting the property value
console.log(person["age"]); //accessing the property value
//this method is great for assigning values to properties you dont know the name of
console.log(person.age);

const sibling = {
    name: "Brittney",
    age: 45,
    occupation: "Accountant",
    favColor: "Blue",
    favSeason: "Winter"
}
//access the keys in sibling
console.log(Object.keys(sibling));
//access the values of the keys in siblings
console.log(Object.values(sibling));

const state = new Object();
state.bird = "Gold Finch";
state.fruit = "Apple";
state.tree = "Western Hemlock";
console.log(Object.keys(state), Object.values(state));



//add a key: value pair to object
state["Marine Mammal"] = "Orca Whale";
const MarineAnimal = state["Marine Animal"];
console.log(Object.keys(state), Object.values(state));

//creating an object with key/values
const country = {
    name: "USA",
    age: 100,
};
let { name, age } = country;
//assigning the keys of country to variables (which house the values which will print)
console.log(name);
console.log(age);

//this will create a new object called car with no key/values
let car = new Object();
//to add key/values...
car.ford = "white";
car["toyota"] = "black";
console.log(car);
//both methods create the keys and values
//see that in the array method, the key is a string (strings are keys by default)
